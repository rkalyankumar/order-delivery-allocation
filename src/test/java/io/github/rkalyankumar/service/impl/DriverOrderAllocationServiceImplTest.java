package io.github.rkalyankumar.service.impl;

import io.github.rkalyankumar.model.*;
import io.github.rkalyankumar.service.DriverOrderAllocationService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class DriverOrderAllocationServiceImplTest {


    private static final Location VELACHERY = new Location(12.9801, 80.2184, "Velachery");
    private static final Location ADAMBAKKAM = new Location(12.9880, 80.2047, "Adambakkam");
    private static final Location MADIPAKKAM = new Location(12.9647, 80.1961, "Madipakkam");
    private static final Location NANGANALLUR = new Location(12.9754, 80.1901, "Nanganallur");
    private static final Location GUINDY = new Location(13.0067, 80.2206, "Guindy");
    private static final Location TNAGAR = new Location(13.0418, 80.2341, "T.Nagar");
    private static final Location WMAMBALAM = new Location(13.0383, 80.2209, "West Mambalam");

    private User user = null;
    private List<Driver> driverList = null;
    private Restaurant restaurant = null;
    private DriverOrderAllocationService allocationService = null;
    private Order order = null;

    private Driver driver1, driver2, driver3, driver4, driver5;


    @Before
    public void setUp() throws Exception {

        User user = new User("Kalyankumar. R", "rkalyankumar@gmail.com", "AGS Colony", VELACHERY);
        restaurant = new Restaurant("Thanjavur Mess", "West Mambalam", WMAMBALAM);
        Item meals = new Item("Lunch Thali", 85.0D, ItemType.IT_VEGETARIAN);
        order = new Order(Arrays.asList(meals), user, restaurant);

        driver1 = new Driver("Sundar", TNAGAR);
        driver2 = new Driver("Rahman", ADAMBAKKAM);
        driver3 = new Driver("Raju", MADIPAKKAM);
        driver4 = new Driver("Adam", GUINDY);
        driver5 = new Driver("Ramasamy", NANGANALLUR);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testAllocate() {
        // Given list of drivers & the Allocation Service
        driverList  = Arrays.asList(driver1, driver2, driver3, driver4, driver5);
        allocationService = new DriverOrderAllocationServiceImpl(driverList);
        // Determine best driver
        Driver bestDriverToDeliverFood = null;
        bestDriverToDeliverFood = allocationService.allocate(order);
        assertTrue(bestDriverToDeliverFood.equals(driver2));
    }

    @Test
    public void testAllocateWithDriversLittleDistantFromUser() {
        // Given list of drivers & the Allocation Service
        driverList  = Arrays.asList(driver1,  driver3, driver4, driver5);
        allocationService = new DriverOrderAllocationServiceImpl(driverList);
        // Determine best driver
        Driver bestDriverToDeliverFood = null;
        bestDriverToDeliverFood = allocationService.allocate(order);
        assertTrue(bestDriverToDeliverFood.equals(driver3));
    }

    @Test
    public void testAllocateWhenOnlyOneDriverIsAvailable() {
        // Given list of drivers & the Allocation Service
        driverList  = Arrays.asList(driver1);
        allocationService = new DriverOrderAllocationServiceImpl(driverList);
        // Determine best driver
        Driver bestDriverToDeliverFood = null;
        bestDriverToDeliverFood = allocationService.allocate(order);
        assertTrue(bestDriverToDeliverFood.equals(driver1));
    }
}