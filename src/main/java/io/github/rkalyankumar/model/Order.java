package io.github.rkalyankumar.model;

import io.github.rkalyankumar.util.DistanceUnit;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Data
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class Order {

    private static final Logger log = Logger.getAnonymousLogger();

    private final List<Item> itemList;
    private final User user;
    private final Restaurant restaurant;

    public Driver allocateDriver(Collection<Driver> drivers) {
        Driver bestDriverToDeliver = null;
        double bestDistanceSoFar = Double.MAX_VALUE;
        double currDistance = 0.0D;
        for (Driver driver : drivers) {
            currDistance = driver.getLocation().distanceTo(this.getUser().getLocation(), DistanceUnit.KILOMETERS);
            log.log(Level.FINE, "Distance from " + driver.getLocation().getLocationName() + " to " + this.getUser().getLocation().getLocationName() + " is " + currDistance);
            if (currDistance < bestDistanceSoFar) {
                bestDistanceSoFar = currDistance;
                bestDriverToDeliver = driver;
            }
        }
        return bestDriverToDeliver;
    }
}
