package io.github.rkalyankumar.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode
@ToString
@RequiredArgsConstructor
public class Restaurant {
    private final String name;
    private final String address;
    private final Location location;
}
