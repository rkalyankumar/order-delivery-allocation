package io.github.rkalyankumar.model;

import lombok.*;

@Data
@EqualsAndHashCode
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Driver {
    private final String name;
    private Location location;
}
