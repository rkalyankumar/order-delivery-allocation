package io.github.rkalyankumar.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class User {
    private final String name;
    private final String email;
    private final String deliveryAddress;
    private final Location location;
}
