package io.github.rkalyankumar.model;

public enum ItemType {
    IT_VEGETARIAN,
    IT_NON_VEGETARIAN
}
