package io.github.rkalyankumar.model;

import io.github.rkalyankumar.util.DistanceUnit;
import io.github.rkalyankumar.util.DistanceUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class Location {
    private final Double latitude;
    private final Double longitude;
    private final String locationName;

    public Double distanceTo(Location other, DistanceUnit distanceUnit) {
        return DistanceUtils.distance(this, other, distanceUnit);
    }
}
