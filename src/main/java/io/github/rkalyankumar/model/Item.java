package io.github.rkalyankumar.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class Item {
    private final String name;
    private final Double price;
    private final ItemType itemType;
}
