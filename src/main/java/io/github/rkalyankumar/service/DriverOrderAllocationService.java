package io.github.rkalyankumar.service;

import io.github.rkalyankumar.model.Driver;
import io.github.rkalyankumar.model.Order;

import java.util.List;

public interface DriverOrderAllocationService {

    public Driver allocate(Order order);
}
