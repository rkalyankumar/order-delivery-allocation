package io.github.rkalyankumar.service.impl;

import io.github.rkalyankumar.model.Driver;
import io.github.rkalyankumar.model.Order;
import io.github.rkalyankumar.service.DriverOrderAllocationService;
import io.github.rkalyankumar.util.DistanceUnit;
import lombok.RequiredArgsConstructor;

import java.util.Collection;

@RequiredArgsConstructor
public class DriverOrderAllocationServiceImpl implements DriverOrderAllocationService {

    private final Collection<Driver> drivers;

    @Override
    public Driver allocate(Order order) {
        return order.allocateDriver(drivers);
    }
}
