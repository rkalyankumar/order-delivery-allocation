package io.github.rkalyankumar.util;

import io.github.rkalyankumar.model.Location;

public abstract class DistanceUtils {

    public static double distance(Location from, Location to, DistanceUnit distanceUnit)
    {
        final Double fromLatitude = from.getLatitude();
        final Double fromLongitude = from.getLongitude();
        final Double toLatitude = to.getLatitude();
        final Double toLongitude = to.getLongitude();

        if (fromLatitude == toLatitude && fromLongitude == toLongitude) {
            return 0.0D;
        } else {
            // Reference: https://www.geodatasource.com/developers/java
            double theta = fromLongitude - toLongitude;
            double dist = Math.sin(Math.toRadians(fromLatitude)) * Math.sin(Math.toRadians(toLatitude))
                    + Math.cos(Math.toRadians(fromLatitude)) * Math.cos(Math.toRadians(toLatitude)) *
                    Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            if (distanceUnit == DistanceUnit.KILOMETERS) {
                dist = dist * 1.609344;
            } else if (distanceUnit == DistanceUnit.MILES) {
                dist = dist * 0.8684;
            } else {
                throw new IllegalArgumentException("Invalid distance unit specified in input");
            }
            return dist;
        }
    }
}
