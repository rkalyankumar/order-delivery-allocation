package io.github.rkalyankumar.util;

public enum DistanceUnit {
    MILES,
    KILOMETERS
}
