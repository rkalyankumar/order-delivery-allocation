package io.github.rkalyankumar;

import io.github.rkalyankumar.model.*;
import io.github.rkalyankumar.service.DriverOrderAllocationService;
import io.github.rkalyankumar.service.impl.DriverOrderAllocationServiceImpl;
import io.github.rkalyankumar.util.DistanceUnit;

import javax.print.attribute.standard.MediaSize;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class Main {

    private static final Logger log = Logger.getAnonymousLogger();

    private static final Location VELACHERY = new Location(12.9801, 80.2184, "Velachery");
    private static final Location ADAMBAKKAM = new Location(12.9880, 80.2047, "Adambakkam");
    private static final Location MADIPAKKAM = new Location(12.9647, 80.1961, "Madipakkam");
    private static final Location NANGANALLUR = new Location(12.9754, 80.1901, "Nanganallur");
    private static final Location GUINDY = new Location(13.0067, 80.2206, "Guindy");
    private static final Location TNAGAR = new Location(13.0418, 80.2341, "T.Nagar");
    private static final Location WMAMBALAM = new Location(13.0383, 80.2209, "West Mambalam");


    public static void main(String[] args) {

        User user = new User("Kalyankumar. R", "rkalyankumar@gmail.com", "AGS Colony", VELACHERY);
        Restaurant a2b = new Restaurant("Adyar Ananda Bhavan", "Velachery", VELACHERY);
        Restaurant thanjavurMess = new Restaurant("Thanjavur Mess", "West Mambalam", WMAMBALAM);
        Item meals = new Item("Lunch Thali", 85.0D, ItemType.IT_VEGETARIAN);
        Order order = new Order(Arrays.asList(meals), user, thanjavurMess);
        Driver driver1 = new Driver("Sundar", TNAGAR);
        Driver driver2 = new Driver("Rahman", ADAMBAKKAM);
        Driver driver3 = new Driver("Raju", MADIPAKKAM);
        Driver driver4 = new Driver("Adam", GUINDY);
        Driver driver5 = new Driver("Ramasamy", NANGANALLUR);

        List<Driver> driverList  = Arrays.asList(driver1, driver2, driver3, driver4, driver5);
        DriverOrderAllocationService allocationService = new DriverOrderAllocationServiceImpl(driverList);
        log.info("Allocated Driver: " + allocationService.allocate(order));
    }
}
